# convert-module
This is a simple shell script that looks for OpenText module zip files in the 'source'
directory, unpacks them in 'staging', converts .oll files to unix ospace files, and then does
dos2unix on everything else. FInally it zips up the newly modified directory to a file like
&lt;modulename&gt;_&lt;maj&gt;_&lt;min&gt;_&lt;rev&gt;-Linux.zip where &lt;modulename&gt; is a name like ccmemlc, and &lt;maj&gt;_&lt;min&gt;_&lt;rev> something
like 24_1_0 (naming convention for OT modules).
new zip file gets dropped into target directory. 
