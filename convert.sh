#!/bin/sh
WORKDIR=`pwd`
SRCDIR=${WORKDIR}/source
DESTDIR=${WORKDIR}/target
STAGING=${WORKDIR}/staging
VERS_MAJ=24
VERS_MIN=1
VERS_REV=0
CMDINST=$(command -v dos2unix)

# Make sure Dos2Unix cmd installed
if [[ ! $CMDINST ]]; then
	echo "Missing dos2unix command. Please ensure this is installed before running"
	exit 1
fi

# Get version info from cmdLine rather than hard coding
if [[ $# -lt 2 ]]; then
	echo "Usage: convert.sh <Majver> <minver> "
	echo "Example: convert.sh 24 3 to convert on any build for version 24.3 0"
	exit 0
fi

VERS_MAJ=$1
VERS_MIN=$2
VERS_REV=0	# Not really used

echo "Performing module conversion on any build for ${VERS_MAJ}_${VERS_MIN}"
	
mkdir -p ${STAGING} 
mkdir -p ${DESTDIR}

cd ${SRCDIR} 
for zipfile in ./ccmemlc_${VERS_MAJ}_${VERS_MIN}_${VERS_REV}-Build[0-9]*.zip 
do
   mv $zipfile ${STAGING}
   cd ${STAGING}
   unzip $zipfile 
   echo "Renaming all .oll files in ${zipfile}..."
   find . -name *.oll -exec sh -c 'f="{}"; mv -- "$f" "${f%.*}"' \;
   echo Converting all other files to UNIX...
   find . -type f -name '*.[a-z]*' -exec dos2unix --keepdate {} + 

#  With all files either renamed or converted, let's zip it up
#  Expectation is that there will be a ccmemlc_<maj>_<Min>_<rev> folder
   dirName=ccmemlc_${VERS_MAJ}_${VERS_MIN}_${VERS_REV}
   newzip=ccmemlc_${VERS_MAJ}_${VERS_MIN}_${VERS_REV}-Linux.zip
   newtar=ccmemlc_${VERS_MAJ}_${VERS_MIN}_${VERS_REV}-Linux.tar.gz

   if [ -d ${dirName} ]
   then
     	echo "Zipping contents of ${dirName} to ${newzip}" 
   	zip -r ${newzip} ${dirName} 

	echo "tar-balling contents of ${dirName} to ${newtar}"
        tar -czf ${newtar} ${dirName}

   	mv ${newzip} ${DESTDIR} 
        mv ${newtar} ${DESTDIR}
        rm -rf ${dirName}
   else
	echo "Cannot find directory to zip" 
   fi
done

